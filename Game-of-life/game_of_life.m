%% Game of Thrones
clc; clear
hold on
n = 30;
axis([0 n 0 n])

for i = 0:1:n
   plot([i i], [0 n])
   plot([0 n], [i i])
end

A = zeros(n)

while 1
    [x, y, k] = ginput(1);
    
    if (k ~= 1) % Om det är inte vänster knapp, avsluta placerings fasen.
       break 
    end    

    x = floor(x) + 1;
    y = floor(y) + 1;
    
    if (x >= 1 && x <= n+1 && y >= 1 && y <= n+1)
        fprintf('X: %.1f | Y: %.1f', x, y); % Debug
        if (A(x, y) == 0)
            fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'g') % Koordinater för en 1x1 ruta
            A(x, y) = 1; % här "vänds" allt ett kvarts varv, eftersom x-ledet hamnar kollonnvis i matrisen(?)
            disp(num_of_neighbours(A, x, y, n))
        else
            fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'white')
            A(x, y) = 0;
        end
    else
        disp('out of bounds :(')
    end   
end

%% tick
% TODO: Flytta till egen funktion
B = zeros(n); % Intemediär matris för nästa game tick
for x = 1:n
    for y = 1:n
        v = A(x, y);
        neigh = num_of_neighbours(A, x, y, n);
        if (v == 0 && neigh == 3)
            B(x, y) = 1;
        end
        if (v == 1 && neigh == 2 || neigh == 3)
            B(x, y) = 1;
        end
    end
end
A = B;
for x = 1:n
    for y = 1:n
        if (A(x, y) == 1)
            fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'g')
        else
            fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'white')
        end
    end
end

%% FULL AUTO!
% <C-c> i command window för att avsluta
while 1
   B = zeros(n);
   for x = 1:n
       for y = 1:n
           v = A(x, y);
           neigh = num_of_neighbours(A, x, y, n);
           if (v == 0 && neigh == 3)
               B(x, y) = 1;
           end
           if (v == 1 && neigh == 2 || neigh == 3)
               B(x, y) = 1;
           end
       end
   end
   A = B;
   for x = 1:n
       for y = 1:n
           if (A(x, y) == 1)
               fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'g')
           else
               fill([x-1 x x x-1 x-1], [y-1 y-1 y y y-1], 'white')
           end
       end
   end
   pause(1);
end