function n = num_of_neighbours(A, x, y, dim)
    n = 0;
    for i = x-1:x+1
        for j = y-1:y+1
            if (i>=1 && i<=dim && j>=1 && j<=dim)
                if (i == x && j == y)
                else
                    if (A(i, j) == 1)
                        n = n + 1;
                    end
                end
            end
        end
    end
end