function []=klot(a,r,n,col,alpha)
% klotyta - ritar ett klot runt en punkt i rummet.
%   Syntax:
%           []=klot(a,r,n,col,alpha)
%   Argument:
%           a - en vektor med medelpunkten f�r klotet.
%           r - klotets radie.
%           n - heltal som anger antal paneler p� klotet.
%           col - textstr�ng som anger f�rg p� klotet.
%           alpha - transparens, alpha=1 solid, alpha=0 transparent.
%   Returnerar:
%           -
%   Beskrivning:
%           -
%   Exempel:
%           r=0.3; n=30; col='g'; alpha=0.7;
%           a=[0; 0; 0]; 
%           klot(a,r,n,col,alpha)

    [S,T]=meshgrid(linspace(0,pi,n),linspace(0,2*pi,2*n));
    X=a(1)+r*sin(S).*cos(T);
    Y=a(2)+r*sin(S).*sin(T);
    Z=a(3)+r*cos(S);
    surf(X,Y,Z,'facecolor',col,'edgecolor','none','facealpha',alpha)
