function [] = ikosaeder2()

    hold on

    w = (1 + sqrt(5)) / 2;
    W = -w;
    m = 1;
    M = -m;
    PA = [0 0 0 0 m m M M w w W W
          m m M M w W w W 0 0 0 0
          w W w W 0 0 0 0 m M m M];
    
    for i=1:12
        xyz = [PA(1, i) PA(2, i) PA(3, i)];
        
        for j=1:12
            abc = [PA(1, j) PA(2, j) PA(3, j)];
            
            iop = xyz - abc;
            d = sqrt(sum(iop.^2));
            
            if d == 2
                for k=1:12
                    bnm = [PA(1, k) PA(2, k) PA(3, k)];
                
                    fgh = xyz - bnm;                  
                    wer = abc - bnm;
                    
                    d1 = sqrt(sum(fgh.^2));
                    d2 = sqrt(sum(wer.^2));
                    
                    if d1 == 2 && d2 == 2
                        klot(xyz, 0.3, 30, 'g', 1);
                        klot(abc, 0.3, 30, 'g', 1);
                        klot(bnm, 0.3, 30, 'g', 1);
                        
                        stav(xyz, abc, 0.1, 30, 'b', 1);
                        stav(xyz, bnm, 0.1, 30, 'b', 1);
                        stav(abc, bnm, 0.1, 30, 'b', 1);
                    end
                end
            end  
        end
    end
end

