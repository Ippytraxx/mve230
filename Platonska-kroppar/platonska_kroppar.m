%% Exempel
%figure(1), clf
axis equal, axis([-2 2 -2 2 -2 2]), axis off, axis vis3d
hold on

a = 2*sqrt(2)/3; b = -sqrt(2)/3; c = sqrt(2/3); d = -1/3;
H = [ a b b 0
      0 c -c 0
      d d d 1 ];

for i = 1:size(H,2)
    text(H(1,i),H(2,i),H(3,i),num2str(i))
end

S = [ 1 2 3
    1 2 4
    1 3 4
    2 3 4 ];

for i=1:size(S,1)
Si=S(i,:); fill3(H(1,Si),H(2,Si),H(3,Si),[0.4 0.5 1],'facealpha',0.8)
end
hold off

material shiny
camlight left, camlight head

%% Uppgift 1
% a
hold on

w = (1 + sqrt(5)) / 2;
W = -w
m = 1;
M = -m;
PA = [ 0 0 0 0 m m M M w w W W
       m m M M w W w W 0 0 0 0
       w W w W 0 0 0 0 m M m M ];

S = ikosaeder();

for i = 1:size(S,1)
    Si=S(i,:); fill3(PA(1,Si),PA(2,Si),PA(3,Si),[0.4 0.5 1],'facealpha',0.8)
end

axis([-3 3 -3 3 -3 3])
axis equal

%%
% b
hold on

w = (1 + sqrt(5)) / 2;
W = -w
m = 1;
M = -m;
H = [ 0 0 0 0 m m M M w w W W
      m m M M w W w W 0 0 0 0
      w W w W 0 0 0 0 m M m M ];

S = ikosaeder();

for i = 1:size(S,1)
    Si = S(i,:); Mi = (H(:,Si(1)) + H(:,Si(2)) + H(:,Si(3))) / 3; % Mitt punkten på en triangel (tyngdpunktsformeln)
    
    Mi=Mi*5; % Skalnings faktor
    
    fill3([H(1,Si(1:2)) Mi(1)],[H(2,Si(1:2)) Mi(2)],[H(3,Si(1:2)) Mi(3)],[0.4 0.5 1],'facealpha',0.8)
    fill3([H(1,Si(2:3)) Mi(1)],[H(2,Si(2:3)) Mi(2)],[H(3,Si(2:3)) Mi(3)],[0.4 0.5 1],'facealpha',0.8)
    fill3([H(1,Si([3,1])) Mi(1)],[H(2,Si([3,1])) Mi(2)],[H(3,Si([3,1])) Mi(3)],[0.4 0.5 1],'facealpha',0.8)
end

%%
% Uppgift 2
ikosaeder2()

