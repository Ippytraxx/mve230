function []=stav(x1,x2,r,n,col,alpha)
% stav - ritar en stav mellan tv� punkter i rummet.
%   Syntax:
%           []=stav(x1,x2,r,n,col,alpha)
%   Argument:
%           x1, x2 - tv� vektor med start och slutpunkt f�r staven.
%           r - stavens radie.
%           n - heltal som anger antal paneler p� stavens yta.
%           col - textstr�ng som anger f�rg p� staven.
%           alpha - transparens, alpha=1 solid, alpha=0 transparent.
%   Returnerar:
%           -
%   Beskrivning:
%           -
%   Exempel:
%           r=0.3; n=30; col='m'; alpha=0.7;
%           x1=[0; 0; 0]; x1=[1 2 1];
%           stav(x1,x2,r,n,col,alpha)

    u=x2-x1; u=u/norm(u); Z=null(u); v=Z(:,1); w=Z(:,2);
    [S,T]=meshgrid(linspace(0,2*pi,n),linspace(0,1,2));
    X=x1(1)+T*(x2(1)-x1(1))+r*cos(S)*v(1)+r*sin(S)*w(1);
    Y=x1(2)+T*(x2(2)-x1(2))+r*cos(S)*v(2)+r*sin(S)*w(2);
    Z=x1(3)+T*(x2(3)-x1(3))+r*cos(S)*v(3)+r*sin(S)*w(3);
    surf(X,Y,Z,'facecolor',col,'edgecolor','none','facealpha',alpha)
