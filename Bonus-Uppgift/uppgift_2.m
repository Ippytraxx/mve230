%% Uppgift 2
f = @(x) 1.5 + sin(0.02 * x.^2);

f_a = @(x) 2 * pi * f(x); % Cirkels radie
f_v = @(x) pi * f(x).^2; % Cirkels area

A = quad(f_a, 0, 25) % Byt till 'integral' för MATLAB
V = quad(f_v, 0, 25)
