%% Uppgift 1
format long
hold on

n = 10000;
f = @(r, p, a) 1 + a*sin(r.^2 .* sin(p) .* cos(p)).^2 - r.^2;
l = linspace(0, 2*pi, n);
r = zeros(n, 5);
colindex = 0;
rowindex = 0;

for a = [0 1 2 3 5]
    colindex = colindex + 1;
    rowindex = 0;
    for i = l
        rowindex = rowindex + 1;
        r(rowindex, colindex) = fzero(@(r)f(r, i, a), 1);
    end
end
c = cos(l); 
s = sin(l);
x = r' .* [c;c;c;c;c];
y = r' .* [s;s;s;s;s];

polylen(x(1, 1:end)', y(1, 1:end)')
polylen(x(2, 2:end)', y(2, 2:end)')
polylen(x(3, 3:end)', y(3, 3:end)')
polylen(x(4, 4:end)', y(4, 4:end)')
polylen(x(5, 5:end)', y(5, 5:end)')

plot(x',y', 'linewidth', 3)
