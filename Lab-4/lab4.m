%% Uppgift 2
l = linspace(0, 2*pi, 100);
R = @(t) (2 + sin(3*t)) ./ sqrt(1 + exp(cos(t)));
Rm = @(t) (2 + sin(3*t)) ./ sqrt(1 + exp(cos(t))) - 1; % R - 1
r = R(l)
xe = cos(l); ye = sin(l); % enhetscirkeln
x = r .* cos(l); y = r .* sin(l); % figuren som ges av r
hold on
plot(xe, ye)
plot(x, y)
axis equal

count = 1
while 1
   [x,y,btn] = ginput(1);
   if btn==1
      fprintf('Point #%d: X: %.4f \tY: %.4f\tAngle: %.2f\n', count, x, y, atan2(y,x))
      plot(x, y, '-o')
      text(x + 0.1, y, num2str(count))
      count = count + 1;
   else
       break
   end
end

z1 = fzero(Rm, [-0.1 0.1])
z2 = fzero(Rm, [pi/4 pi/2])
z3 = fzero(Rm, [pi/2 3/4*pi])
z4 = fzero(Rm, [pi 4.8/4*pi])
z5 = fzero(Rm, [4.7/4*pi 3/2*pi])
z6 = fzero(Rm, [3/2*pi 7.2/4*pi])

%% Uppgift 3
y = @(u)((sin(u)).^2)./(u.^2);
l = linspace(-10, 10, 1000);
plot(l, y(l))

%% Uppgift 4
