function x = min_noll(x)

    f=@(x)x.^3-cos(4*x);
    Df=@(x)3*x.^2 + 4*sin(4*x);
    kmax = 10;
    tol = 0.5e-8;

    for k = 1:kmax
        
        plot(x, f(x), 'g-o');
        
        h = -f(x)/Df(x);
        x = x + h;
        
        if abs(h) < tol
            break;
        end
    end
    plot(x, f(x), 'r-o');
end

