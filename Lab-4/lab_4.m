%%
% Uppgift 1
hold on

f=@(x)x.^3-cos(4*x);

p = linspace(-2, 2);

plot(p, f(p));

grid on
axis([-2 2 -0.5 0.5])

min_noll(-1)
min_noll(-0.5)
min_noll(0.5)

%%
% Uppgift 2

l = linspace(0, 2*pi, 100);
R = @(t) (2 + sin(3*t)) ./ sqrt(1 + exp(cos(t)));
Rm = @(t) (2 + sin(3*t)) ./ sqrt(1 + exp(cos(t))) - 1; % R - 1
r = R(l)
xe = cos(l); ye = sin(l); % enhetscirkeln
x = r .* cos(l); y = r .* sin(l); % figuren som ges av r
hold on
plot(xe, ye)
plot(x, y)
axis equal

count = 1
while 1
   [x,y,btn] = ginput(1);
   if btn==1
      fprintf('Point #%d: X: %.4f \tY: %.4f\tAngle: %.2f\n', count, x, y, atan2(y,x))
      plot(x, y, '-o')
      text(x + 0.1, y, num2str(count))
      count = count + 1;
   else
       break
   end
end

z1 = fzero(Rm, [-0.1 0.1])
z2 = fzero(Rm, [pi/4 pi/2])
z3 = fzero(Rm, [pi/2 3/4*pi])
z4 = fzero(Rm, [pi 4.8/4*pi])
z5 = fzero(Rm, [4.7/4*pi 3/2*pi])
z6 = fzero(Rm, [3/2*pi 7.2/4*pi])

%%
% Uppgift 3
f = @(x)(sin(x).^2)./(x.^2);
p = linspace(-10, 10, 10000);
plot(p, f(p));

 axis([-10 10 -1 1])
 
%%
% Uppgift 4

p = linspace(0, 1, 100);

f = @(x)(sin(x) .* x)

b = 1 / 99;

A_v = sum(f(p(1, 1:end - 1)) * b);
A_h = sum(f(p(1, 2:end)) * b);
A_m = sum((f(p(1: 1:end -1) + f(p(1, 2:end))) ./ 2) * b);
A_t = sum((f(p(1: 1:end - 1)) + f(p(1, 2:end))) * b/2);

fprintf('Vänster %.2f\n', A_v);
fprintf('Höger %.2f\n', A_h);
fprintf('Mittpunkt %.2f\n', A_m);
fprintf('Trapets %.2f\n', A_t);

%%
% Uppgift 5

hold on

f = @(x)exp(-(x.^2)/2);
g = @(x)x.^2 - 3 * x + 2;

fg = @(x)f(x) - g(x);

n_1 = fzero(fg, [0.4 0.6])
n_2 = fzero(fg, [1.8 2.2])

p = linspace(-10, 10);

a = integral(fg, n_1, n_2)

plot(p, f(p));
plot(p, g(p), 'g');

%%
% Uppgift 6

f = @(t, u)cos(3 * t) - sin(5 * t) * u;
tspan = [0 15];
u_0 = 2;

[t, U] = ode45(f, tspan, u_0);

plot(t, U);

%%
% Uppgift 7

hold on
grid on

m = 1;
l = 0.1;
g = 9.82;

tspan = [0 10];

f = @(t, u)[-m*g*sin(u(2)); u(1)];

for i = pi / 6:pi / 12:pi / 3
    u_0 = [0; i];

    [t, U] = ode45(@(t,u)f(t, u), tspan, u_0);
    
    disp(U)

    plot(t, U);
end
