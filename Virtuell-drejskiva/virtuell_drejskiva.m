%%
% Virtuell drejskiva

hold on
axis equal
axis ([-2 2 -2 2 -6 6])

X = [];
Y = [];
Z = [];

while 1
    [x, y, knapp] = ginput(1);
    
    p = linspace(0, 2*pi, 100);
    
    if knapp == 1
        plot(x, y, 'ob');
        plot(-x, y, 'ob');
        
        X = [X; x * cos(p)];
        Y = [Y; x * sin(p)];
        Z = [Z; y * ones(size(p))]
    else
        surf(X, Z, Y);
        break;
    end
end
